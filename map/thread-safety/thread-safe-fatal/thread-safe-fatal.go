package main

// 场景: 2个协程同时读和写，以下程序会出现致命错误：fatal error: concurrent map writes
import (
	"fmt"
	"time"
)

func main() {
	s := make(map[int]int)
	for i := 0; i < 100; i++ {
		go func(i int) {
			s[i] = i
		}(i)
	}
	for i := 0; i < 100; i++ {
		go func(i int) {
			fmt.Printf("map第%d个元素值是%d\n", i, s[i])
		}(i)
	}
	time.Sleep(100 * time.Second)
}

// fatal error: concurrent map writes
