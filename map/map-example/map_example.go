package main

/*
Go语言中读取map有两种语法：带comma和不带comma。
当要查询的key不在map里，带comma的用法会返回一个bool型变量提示key是否在map中；
而不带comma的语句则会返回一个value类型的零值。
*/
m := map[string]string{"name": "ann", "age": "14"}

// 不带comma
value := m["name"]
fmt.Println("value:%s", value)
// 带comma
value, ok := m["age"]
if ok {
	fmt.Printf("value:%s", value)
}

/*
查找流程：
1.写保护监测。函数首先会检查 map 的标志位 flags。如果 flags 的写标志位此时被置 1 了，说明有其他协程在执行“写”操作，进而导致程序 panic，这也说明了 map 不是线程安全的。
2.计算hash值。
3.找到hash对应的bucket。
4.判断是否在扩容，如果在扩容中需要先从旧的bucket中查询。
5.遍历bucket查找。
6. 返回key对应的指针
*/

// keys的偏移量
dataOffset = unsafe.Offsetof(struct{
	b bmap
	v int64
}{}.v)
  
// 一个bucket的元素个数
bucketCnt = 8

// key 定位公式
k :=add(unsafe.Pointer(b),dataOffset+i*uintptr(t.keysize))

// value 定位公式
v:= add(unsafe.Pointer(b),dataOffset+bucketCnt*uintptr(t.keysize)+i*uintptr(t.valuesize))
