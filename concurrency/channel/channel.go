package main

import (
	"fmt"
	"time"
)

func main() {
	ch := make(chan struct{}, 1)
	for i := 0; i < 10; i++ {
		go func() {
			ch <- struct{}{}
			time.Sleep(1 * time.Second)
			fmt.Println("通过ch访问临界区")
			<-ch
		}()
	}
	for {
	} // this loop will spin, using 100% CPU
}

// 通过ch访问临界区
// 通过ch访问临界区
// 通过ch访问临界区
// 通过ch访问临界区
// 通过ch访问临界区
// 通过ch访问临界区
// ......
