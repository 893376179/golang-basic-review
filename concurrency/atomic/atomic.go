// 原子操作
package main

import (
	"fmt"
	"sync/atomic"
)

var ops int64 = 0

func main() {
	add(&ops, 3)
	load(&ops)
	compareAndSwap(&ops, 3, 4)
	swap(&ops, 5)
	store(&ops, 6)
}

func add(addr *int64, delta int64) {
	atomic.AddInt64(addr, delta)     // 加操作
	fmt.Println("add opts: ", *addr) // add opts:  3
}

func load(addr *int64) {
	fmt.Println("load opts: ", atomic.LoadInt64(addr)) // load opts:  3
}

func compareAndSwap(addr *int64, oldValue, newValue int64) {
	if atomic.CompareAndSwapInt64(addr, oldValue, newValue) {
		fmt.Println("caps opts: ", *addr) // caps opts:  4
		return
	}
}

func swap(addr *int64, newValue int64) {
	atomic.SwapInt64(addr, newValue)
	fmt.Println("swap opts: ", *addr) // swap opts:  5
}

func store(addr *int64, newValue int64) {
	atomic.StoreInt64(addr, newValue)
	fmt.Println("store opts: ", *addr) // store opts:  6
}
