package main

/*
sync.Mutex互斥锁可以限制对临界资源的访问，保证只有一个goroutine访问共享资源
使用场景：大量读写，比如多个goroutine并发更新同一个资源。
*/
import (
	"fmt"
	"sync"
)

type Counter struct {
	mutex sync.Mutex
	count uint64
}

func (c *Counter) Incr() {
	c.mutex.Lock()
	c.count++
	c.mutex.Unlock()
}

func (c *Counter) Count() uint64 {
	c.mutex.Lock()
	defer c.mutex.Unlock()
	return c.count
}

func main() {
	var counter Counter
	var wg sync.WaitGroup
	var gNum = 100
	wg.Add(gNum)
	for i := 0; i < gNum; i++ {
		go func() {
			defer wg.Done()
			counter.Incr() // 收到锁保护的方法
		}()
	}
	wg.Wait()
	fmt.Println(counter.Count()) // 100
}
