package main

import (
	"fmt"
	"sync"
)

/*
sync.Pool可以将暂时将不用的对象缓存起来，待下次需要的时候直接使用，不用再次经过内存分配，
复用对象的内存，减轻 GC 的压力，提升系统的性能。
使用场景：对象池化， TCP连接池、数据库连接池、Worker Pool
*/

func main() {
	pool := sync.Pool{
		New: func() interface{} {
			return 0
		},
	}
	for i := 0; i < 10; i++ {
		v := pool.Get().(int)
		fmt.Println(v)
		pool.Put(i)
	}
}

// 0 // 取出来的值是put进去的，对象复用；如果是新建对象，则取出来的值为0
// 0
// 1
// 2
// 3
// 4
// 5
// 6
// 7
// 8
