package main

import (
	"fmt"
	"sync"
	"sync/atomic"
	"time"

	"golang.org/x/sync/singleflight"
)

/*
singleflight用于抑制对下游的重复请求
使用场景：访问缓存、数据库等场景，缓存过期时只有一个请求去更新数据库。
*/

var count int32

// 模拟从数据库读区
func getArticle(id int) (article string, err error) {
	// 假设这里会对数据库进行调用，模拟不同并发下耗时不同
	atomic.AddInt32(&count, 1)
	time.Sleep(time.Duration(count) * time.Millisecond)
	return fmt.Sprintf("Article: %d", id), nil
}

func singleflightGetArticle(sg *singleflight.Group, id int) (article string, err error) {
	v, err, _ := sg.Do(fmt.Sprintf("%d", id), func() (interface{}, error) {
		return getArticle(id)
	})
	return v.(string), err
}

func main() {
	time.AfterFunc(1*time.Second, func() {
		atomic.AddInt32(&count, -count)
	})
	var (
		wg  sync.WaitGroup
		now = time.Now()
		n   = 1000
		sg  = &singleflight.Group{}
	)
	for i := 0; i < n; i++ {
		wg.Add(1)
		go func() {
			res, _ := singleflightGetArticle(sg, 1)
			if res != "Article: 1" {
				panic("err")
			}
			wg.Done()
		}()
	}
	wg.Wait()
	fmt.Printf("同时发起%d次请求，耗时：%s", n, time.Since(now))
}

// 同时发起1000次请求，耗时：3.728427ms
