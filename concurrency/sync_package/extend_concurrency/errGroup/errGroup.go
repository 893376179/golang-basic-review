package main

import (
	"fmt"
	"net/http"

	"golang.org/x/sync/errgroup"
)

/*
errgroup可以在一组Goroutine中提供同步、错误传播以及上下文取消的功能
使用场景：只要一个goroutine出错，就不再等待其他goroutine了，减少资源浪费，并且返回错误。
*/
func main() {
	var g errgroup.Group
	var urls = []string{
		"http://www.baiduewde.com",
		"http://www.sina.com",
	}
	for i := range urls {
		url := urls[i]
		g.Go(func() error {
			resp, err := http.Get(url)
			if err == nil {
				resp.Body.Close()
			} else {
				fmt.Println("fetched error: ", err.Error())
			}

			return err
		})
	}
	err := g.Wait()
	if err == nil {
		fmt.Println("Successfully fetched all urls")
	} else {
		fmt.Println("fetched error: ", err.Error())
	}
}

// fetched error:  Get "http://www.baiduewde.com": dial tcp: lookup www.baiduewde.com: no such host
// fetched error:  Get "http://www.baiduewde.com": dial tcp: lookup www.baiduewde.com: no such host

// 如果网址正确：Successfully fetched all urls
