package main

import (
	"fmt"
	"sync"
)

/*
sync.Map线程安全的map
使用场景：map并发读写
*/

func main() {
	var scene sync.Map
	scene.Store("1", 1)
	scene.Store("2", 2)
	scene.Store("3", 3)
	fmt.Println(scene.Load("2")) // 2 true
	scene.Delete("2")
	scene.Range(func(k, v interface{}) bool {
		fmt.Println("iterate:", k, v)
		return true
	})
	// iterate: 1 1
	// iterate: 3 3
}
