package main

import (
	"fmt"
	"sync"
	"time"
)

/*
sync.Mutex读写锁可以限制临界资源访问，保证只有一个goroutine写共享资源，可以有多个goroutine读共享资源。
使用场景：大量并发读，少量并发写，有强烈的性能要求。
*/

type Counter struct {
	mutex sync.RWMutex
	count uint64
}

func (c *Counter) Incr() {
	c.mutex.Lock()
	c.count++
	c.mutex.Unlock()
}

func (c *Counter) Count() uint64 {
	c.mutex.RLock()
	defer c.mutex.RUnlock()
	return c.count
}

func main() {
	var counter Counter
	var gNum = 100
	for i := 0; i < gNum; i++ {
		go func() {
			counter.Count()
		}()
	}
	for {
		// 一个写writer
		counter.Incr()
		fmt.Println("incr")
		time.Sleep(1 * time.Second)
	}
}

// incr
// incr
// incr
// incr
// incr
// incr
// incr
// ......
