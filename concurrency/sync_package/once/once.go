package main

import (
	"fmt"
	"sync"
)

/*
sync.Once 可以保证在 Go 程序运行期间的某段代码只会执行一次
使用场景：常常用于单例对象的初始化场景
*/
func main() {
	o := &sync.Once{}
	for i := 0; i < 10; i++ {
		o.Do(func() {
			fmt.Println("only once")
		})
	}
}

// only once
