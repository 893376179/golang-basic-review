Go标准库提供了Cond原语，可以让Goroutine在满足条件时被阻塞和唤醒。

底层数据结构：

type Cond struct {
    noCopy noCopy

    // L is held while observing or changing the condition
    L Locker

    notify  notifyList
    checker copyChecker
}

type notifyList struct {
    wait   uint32
    notify uint32
    lock   uintptr // key field of the mutex
    head   unsafe.Pointer
    tail   unsafe.Pointer
}

checker：用于禁止运行期间发生拷贝。

L：可以传入一个读写锁或者互斥锁，当修改条件或者调用wait方法时需要加锁。

notify：通知链表，调用Wait()方法的Goroutine会被放到这个链表中，从这里获取需被唤醒的Goroutine列表。


在Cond中主要有以下方法：

* `sync.NewCond(l Locker)`: 新建一个 sync.Cond 变量，注意该函数需要一个 Locker 作为必填参数，这是因为在 `cond.Wait()` 中底层会涉及到 Locker 的锁操作。
* `Cond.Wait()`: 阻塞等待被唤醒，调用Wait函数前 **需要先加锁** ；并且由于Wait函数被唤醒时存在虚假唤醒等情况，导致唤醒后发现，条件依旧不成立，因此需要使用 for 语句来循环地进行等待，直到条件成立为止。
* `Cond.Signal()`: 只唤醒一个最先 Wait 的 goroutine，可以不用加锁。
* `Cond.Broadcast()`: 唤醒所有Wait的goroutine，可以不用加锁。


```go
package main

import (
    "fmt"
    "sync"
    "sync/atomic"
    "time"
)

var status int64

func main(){
    c := sync.NewCond(&sync.Mutex{})
    for i := 0; i < 10; i ++{
        go listen(c)
    }
    go broadcast(c)
    time.Sleep(1 * time.Second)
}

func broadcast(c *sync.Cond) {
    // 原子操作
    atomic.StoreInt64(&status, 1)
    c.Broadcast()
}

func listen(c *sync.Cond) {
    c.L.Lock()
    for atomic.LoadInt64(&status) != 1 {
        c.Wait() // Wait 内部会先调用 c.L.Unlock()，来先释放锁，如果调用方不先加锁的话，会报错
    }
    fmt.Println("listen")
    c.L.Unlock()
}
```
