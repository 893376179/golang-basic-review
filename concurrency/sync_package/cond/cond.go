package main

import (
	"fmt"
	"sync"
	"sync/atomic"
	"time"
)

/*
sync.Cond可以让一组的goroutine都在满足特定条件时被唤醒
使用场景：利用等待/通知机制实现阻塞或者唤醒
*/

var status int64

func main() {
	c := sync.NewCond(&sync.Mutex{}) // return &Cond{L: l}
	for i := 0; i < 10; i++ {
		go listen(c)
		fmt.Println("111")
	}
	time.Sleep(1 * time.Second)
	go broadcast(c)
	time.Sleep(1 * time.Second)
}

func listen(c *sync.Cond) {
	c.L.Lock()
	for atomic.LoadInt64(&status) != 1 {
		c.Wait() // 阻塞
	}
	fmt.Println("listen")
	c.L.Unlock()
}

func broadcast(c *sync.Cond) {
	c.L.Lock()
	atomic.StoreInt64(&status, 1)
	c.Signal()
	fmt.Println("broadcast")
	c.L.Unlock()
}

// 111
// 111
// 111
// 111
// 111
// 111
// 111
// 111
// 111
// 111
// broadcast
// listen
