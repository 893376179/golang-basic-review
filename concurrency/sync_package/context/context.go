package main

import (
	"context"
	"fmt"
	"time"
)

/*
sync.Context 可以进行上下文信息传递、提供超时和取消机制、控制子 goroutine 的执行。
使用场景：取消一个goroutine的执行
*/
func main() {
	ctx, cancel := context.WithCancel(context.Background())
	go func() {
		defer func() {
			fmt.Println("goroutine exit")
		}()
		for {
			select {
			case <-ctx.Done():
				fmt.Println("receive cancel signal!")
				return
			default:
				fmt.Println("default")
				time.Sleep(1 * time.Second)
			}
		}
	}()
	time.Sleep(10 * time.Second)
	cancel()
	time.Sleep(2 * time.Second)
}

// default
// default
// default
// default
// default
// default
// default
// default
// default
// default
// receive cancel signal!
// goroutine exit
