package main

import (
	"fmt"
	"net/http"
	"sync"
)

/*
sync.WaitGroup可以等待一组goroutine的返回
使用场景：并发等待，任务编排，一个比较常见的场景是批量发出RPC或者HTTP请求
*/

type httpPkg struct{}

func (p *httpPkg) Get(url string) {}

func main() {
	var wg sync.WaitGroup
	var urls = []string{
		"http://www.baidu.com",
		"http://www.bilibili.com",
	}
	for _, url := range urls {
		wg.Add(1)
		go func(url string) {
			defer wg.Done()
			http.Get(url)
		}(url)
	}
	fmt.Println("request success")
	wg.Wait()
}
