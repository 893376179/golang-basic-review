Go标准库提供了WaitGroup原语，可以用它来等待一批Goroutine结束。

底层数据结构：

```
// A WaitGroup must not be copied after first use.
type WaitGroup struct {
 noCopy noCopy
 state1 [3]uint32
}
```

其中noCopy是golang源码中检查禁止拷贝的技术。如果程序中有waitGroup的赋值行为，使用go vet检查程序时会有报错。noCopy不会影响程序的正常编译和运行。

state1主要存储状态和新号量，状态维护了2个计数器，一个是请求计数器counter，另外一个是等待计数器waiter（已调用waitGroup.Wait的goroutine个数）。

使用方法：

在waitGroup中主要有3个方法：

* WaitGroup.Add():
  用来添加或减少请求的goroutine数量，Add(n)即执行操作counter += n。
* WaitGroup.Done():
  相当于Add(-1)，Done()导致counter -= 1，请求计数器counter为0时通过调用runtime_Semrelease唤醒waiter线程。
* WaitGroup.Wait():

    将waiter++，同时通过新号量调用runtime_Semacquire(semap)阻塞当前goroutine。


```go
func main() {
    var wg sync.WaitGroup
    for i := 1; i <= 5; i++{
	wg.Add(1)
	go func(){
	    defer wg.Done()
	    fmt.Println("hello")
	}()
    }
    wg.Wait()
}
```
