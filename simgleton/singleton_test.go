package singleton

import (
	"fmt"
	"testing"
)

func TestSingleton(t *testing.T) {
	inst1 := GetInstance()
	inst2 := GetInstance()
	if inst1 == inst2 {
		fmt.Println("instance is equal")
	}
}
