package singleton

import "sync"

type Singleton struct{}

var s *Singleton
var once sync.Once

func GetInstance() *Singleton {
	once.Do(func() {
		s = &Singleton{}
	})
	return s
}
