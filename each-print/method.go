package method

import (
	"fmt"
	"sync"
)

func Method1() {
	wg := &sync.WaitGroup{}
	wg.Add(2)

	ch1 := make(chan int, 1)
	ch2 := make(chan int, 1)

	go printA(ch1, ch2, wg)
	go printB(ch1, ch2, wg)

	ch1 <- 888
	wg.Wait()

	fmt.Println("finished")
}

func printA(ch1, ch2 chan int, wg *sync.WaitGroup) {
	defer wg.Done() // 用来告诉主协程这个go程执行完毕
	for i := 1; i < 101; i++ {
		<-ch1
		if i%2 == 1 {
			fmt.Println("goroutine A is printing: ", i)
		}
		ch2 <- 888
	}
}

func printB(ch1, ch2 chan int, wg *sync.WaitGroup) {
	defer wg.Done()
	for i := 1; i < 101; i++ {
		<-ch2
		if i%2 == 0 {
			fmt.Println("goroutine B is printing: ", i)
		}
		ch1 <- 999
	}

}
