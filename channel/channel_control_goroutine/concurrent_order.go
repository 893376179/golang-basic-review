package main

import (
	"fmt"
	"sync"
	"time"
)

/*
多个goroutine并发执行时，每一个goroutine抢到处理器的时间点不一致，gorouine的执行本身不能保证顺序。
使用channel进行通信通知，用channel去传递信息，从而控制并发执行顺序。
*/
var wg sync.WaitGroup

func main() {
	ch1 := make(chan struct{}, 1)
	ch2 := make(chan struct{}, 1)
	ch3 := make(chan struct{}, 1)
	ch1 <- struct{}{}
	wg.Add(3)
	start := time.Now().Unix()
	go print("goroutine1", ch1, ch2)
	go print("goroutine2", ch2, ch3)
	go print("goroutine3", ch3, ch1)
	wg.Wait()
	end := time.Now().Unix()
	fmt.Printf("duration:%d\n", end-start)
}

func print(goroutine string, inputchan chan struct{}, outputchan chan struct{}) {
	for i := 0; i < 3; i++ {
		time.Sleep(1 * time.Second)
		select {
		case <-inputchan:
			fmt.Printf("%s\n", goroutine)
			outputchan <- struct{}{}
		}
	}
	wg.Done()
}

// goroutine1
// goroutine2
// goroutine3
// goroutine1
// goroutine2
// goroutine3
// goroutine1
// goroutine2
// goroutine3
// duration:3
