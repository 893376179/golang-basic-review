package main

import (
	"fmt"
	"time"
)

/*
死锁：多个协程执行过程中，由于竞争资源或彼此通信而造成的一种阻塞的现象。
channel死锁场景：
	1.非缓存channel只写不读
	2.非缓存channel读在写后面
	3.缓存channel写入超过缓冲区数量
	4.空读
	5.多个协程互相等待
*/
func deadlock1() {
	ch := make(chan int)
	ch <- 3
}

func deadlock2() {
	ch := make(chan int)
	ch <- 100
	go func() {
		num := <-ch
		fmt.Println("num=", num)
	}()
	time.Sleep(1 * time.Second)
}

func deadlock3() {
	ch := make(chan int, 3)
	ch <- 3
	ch <- 4
	ch <- 5
	ch <- 6
}

func deadlock4() {
	ch := make(chan int)
	fmt.Println(<-ch)
}

func deadlock5() {
	ch1 := make(chan int)
	ch2 := make(chan int)

	go func() {
		for {
			select {
			case num := <-ch1:
				fmt.Println("num=", num)
				ch2 <- 100
			}
		}
	}()
	for {
		select {
		case num := <-ch2:
			fmt.Println("num=", num)
			ch1 <- 200
		}
	}

}
