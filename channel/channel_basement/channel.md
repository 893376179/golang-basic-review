Go中channel是一个队列，先进先出，负责协程间通信。（Go提倡不要用共享内存通信，而要通过通信来实现内存共享）
使用场景：
    停止信号监听
    定时任务
    生产方和消费方解耦
    控制并发数

底层数据结构：
通过var、make函数创建的channel变量是一个存储在函数栈帧上的指针，占用8个字节，指向堆上的hchan结构体。
type hchan struct {
    closed   uint32   // channel是否关闭的标志
    elemtype *_type   // channel中的元素类型
    
    // channel分为无缓冲和有缓冲两种。
    // 对于有缓冲的channel存储数据，使用了 ring buffer（环形缓冲区) 来缓存写入的数据，本质是循环数组
    // 为啥是循环数组？普通数组不行吗，普通数组容量固定更适合指定的空间，弹出元素时，普通数组需要全部都前移
    // 当下标超过数组容量后会回到第一个位置，所以需要有两个字段记录当前读和写的下标位置
    buf      unsafe.Pointer // 指向底层循环数组的指针（环形缓冲区）
    qcount   uint           // 循环数组中的元素数量
    dataqsiz uint           // 循环数组的长度
    elemsize uint16                 // 元素的大小
    sendx    uint           // 下一次写下标的位置
    recvx    uint           // 下一次读下标的位置
    
    // 尝试读取channel或向channel写入数据而被阻塞的goroutine
    recvq    waitq  // 读等待队列
    sendq    waitq  // 写等待队列

    lock mutex //互斥锁，保证读写channel时不存在并发竞争问题
}

其中等待队列是双向链表，包含一个头结点和一个尾结点。
每个节点是一个sudog结构体变量，记录哪个协程在等待，等待的是哪个channel，等待发送/接收的数据在哪里

操作：
创建channel:
使用make(chan T, cap)
    // 带缓冲
    ch := make(chan int, 3)
    // 不带缓冲
    ch := make(chan int)

发送：
发送操作，编译时转换为runtime.chansend函数。
阻塞式： ch<-10
非阻塞式： 
    select { 
        case ch<- 10:
        ...
        default
    }

向 channel 中发送数据时大概分为两大块：检查和数据发送，数据发送流程如下：
如果 channel 的读等待队列存在接收者goroutine:
    将数据直接发送给第一个等待的 goroutine， 唤醒接收的 goroutine
如果 channel 的读等待队列不存在接收者goroutine:
    如果循环数组buf未满，那么将会把数据发送到循环数组buf的队尾
    如果循环数组buf已满，这个时候就会走阻塞发送的流程，将当前 goroutine 加入写等待队列，并挂起等待唤醒


接收：
接收操作，编译时转换为runtime.chanrecv函数。
阻塞式：
    <-ch
    v := <-ch
    v, ok := <-ch
    // 当channel关闭时，for循环会自动退出，无需主动监测channel是否关闭，可以防止读取已经关闭的channel,造成读到数据为通道所存储的数据类型的零值
    for i := range ch {
        fmt.Println(i)
    }
非阻塞式：
    select {
        case<-ch:
        ...
        default
    }

关闭：
调用close函数，编译时转换为runtime.closechan函数
close(ch)

总结hchan结构体主要组成部分有四个：
1.保存goroutine直接传递数据的循环数组：buf
2.记录循环数组当前发送或者接收数据的下标值：sendx和recvx
3.保存向该chan发送和从该chan接收数据被阻塞的goroutine队列：sendq 和 recvq
4.保证chan写入和读取数据时线程安全的锁：lock

注意点：
1.一个 channel不能多次关闭，会导致painc；
2.如果多个goroutine都监听同一个channel，那么channel上的数据都可能随机被某一个goroutine取走进行消费；
3.如果多个goroutine监听同一个channel，如果这个channel被关闭，则所有goroutine都能收到退出信号。

channel如何实现线程安全的？
channel的底层实现中，hchan结构体中采用Mutex锁来保证数据读写安全。在对循环数组buf中的数据进行入队和出队操作时，必须先获取互斥锁，才能操作channel数据