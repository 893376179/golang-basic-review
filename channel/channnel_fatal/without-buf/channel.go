package main

import (
	"fmt"
	"time"
)

func loop(ch chan int) {
	for {
		select {
		case i := <-ch:
			fmt.Println("this value of unbuffer channel", i)
		}
	}
}

func main() {
	ch := make(chan int)
	ch <- 1 // ch<-1 发送了，但是同时没有接收者，所以就发生了阻塞
	go loop(ch)
	time.Sleep(1 * time.Second)
}

// fatal error: all goroutines are asleep - deadlock!
