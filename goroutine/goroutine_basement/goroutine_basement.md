Goroutine可以理解为Go语言的协程（轻量级的线程），是Go支持高并发的基础，属于用户态的线程，由Go runtime管理而不是操作系统。

底层数据结构：

type g struct {
    goid    int64 // 唯一的goroutine的ID
    sched gobuf // goroutine切换时，用于保存g的上下文
    stack stack // 栈
    gopc        // pc of go statement that created this goroutine
    startpc    uintptr // pc of goroutine function
    ...
}

type gobuf struct {
    sp   uintptr // 栈指针位置
    pc   uintptr // 运行到的程序位置
    g    guintptr // 指向 goroutine
    ret  uintptr  // 保存系统调用的返回值
    ...
}

type stack struct {
    lo uintptr // 栈的下界内存地址
    hi uintptr // 栈的上界内存地址
}

创建：通过go关键字分调用底层函数runtime.newproc()创建一个goroutine。创建好的goroutine会新建一个自己的栈空间，同时在G的sched中维护栈地址和程序计数器这些信息。

每个G被创建之后会被优先放到本地队列中，如果本地队列满了，就会被放到全局队列中。


运行：goroutine本身是一个数据结构，真正让goroutine运行起来的是调度器。Go实现了一个用户态的调度器（GMP模型）。

![1661696397055](image/goroutine_basement/1661696397055.png)

流程描述：

每个 M 开始执行 P 的本地队列中的 G时，goroutine会被设置成 `running`状态。

如果某个 M 把本地队列中的G都执行完成之后，然后就会去全局队列中拿 G，这里需要注意，每次去全局队列拿 G 的时候，都需要上锁，避免同样的任务被多次拿。

如果全局队列都被拿完了，而当前 M 也没有更多的 G 可以执行的时候，它就会去其他 P 的本地队列中拿任务，这个机制被称之为 work stealing 机制，每次会拿走一半的任务，向下取整，比如另一个 P 中有 3 个任务，那一半就是一个任务。

当全局队列为空，M 也没办法从其他的 P 中拿任务的时候，就会让自身进入自旋状态，等待有新的 G 进来。最多只会有 GOMAXPROCS 个 M 在自旋状态，过多 M 的自旋会浪费 CPU 资源。


阻塞：channel的读写操作、等待锁、等待网络数据、系统调用等都有可能发生阻塞，会调用底层函数runtime.gopark()，会让出CPU时间片，让调度器安排其他等待的任务运行，并在下次某个时候从该位置恢复执行。

当调用该函数之后，goroutine会被设置成waiting状态。


唤醒：处于waiting状态的goroutine，在调用runtime.goready()函数之后被唤醒，唤醒的goroutine会被重新放到发M对应的上下文P对应的runqueue中等待调度。

当调用该函数之后，goroutine会被设置成runnable状态。


退出：当goroutine执行完成后，会调用底层函数runtime.Goexit()。

当调用该函数之后，goroutine会被设置成dead状态。


goroutine和线程的区别：

|            | goroutine                                                                                                   | 线程                                                                                                           |
| ---------- | ----------------------------------------------------------------------------------------------------------- | -------------------------------------------------------------------------------------------------------------- |
| 内存占用   | 创建一个 goroutine 的栈内存消耗为 2 KB。<br />实际运行过程中，如果栈空间不够用，会自动进行扩容              | 创建一个线程的栈内存消耗为 1 MB                                                                                |
| 创建和销毁 | goroutine 因为是由 Go runtime 负责管理的，<br />创建和销毁的消耗非常小，是用户级。                          | 线程创建和销毀都会有巨大的消耗，因为要和操作系统打交道，<br />是内核级的，通常解决的办法就是线程池             |
| 切换       | goroutines 切换只需保存三个寄存器：PC、SP、BP。<br />goroutine 的切换约为 200 ns，相当于 2400-3600 条指令。 | 当线程切换时，需要保存各种寄存器，以便恢复现场。<br />线程切换会消耗 1000-1500 ns，相当于 12000-18000 条指令。 |
