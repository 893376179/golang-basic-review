package main

import (
	"fmt"
	"runtime"
	"sync"
)

// 任务发送和执行分离，指定消费者并发协程数
var wg = sync.WaitGroup{}

func main() {
	requestCount := 10
	fmt.Println("goroutine_num", runtime.NumGoroutine())
	ch := make(chan bool)
	for i := 0; i < 3; i++ {
		go read(ch, i) // 由于channel无缓存，所以代码顺序将读放到写之前。否则会导致死锁。
	}
	for i := 0; i < requestCount; i++ {
		wg.Add(1)
		ch <- true
	}

	wg.Wait()
}

func read(ch chan bool, i int) {
	for range ch {
		fmt.Printf("goroutine_num: %d, go func: %d\n", runtime.NumGoroutine(), i)
		wg.Done()
	}
}

// goroutine_num 1
// goroutine_num: 4, go func: 2
// goroutine_num: 4, go func: 2
// goroutine_num: 4, go func: 2
// goroutine_num: 4, go func: 2
// goroutine_num: 4, go func: 2
// goroutine_num: 4, go func: 2
// goroutine_num: 4, go func: 2
// goroutine_num: 4, go func: 2
// goroutine_num: 4, go func: 2
// goroutine_num: 4, go func: 2
