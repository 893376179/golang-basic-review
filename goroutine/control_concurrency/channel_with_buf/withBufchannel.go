package main

import (
	"fmt"
	"runtime"
	"sync"
)

// 利用缓冲满时发送阻塞的特性
var wg = sync.WaitGroup{}

func main() {
	// 模拟用户请求
	requestCount := 10
	fmt.Println("goroutine_num", runtime.NumGoroutine())
	ch := make(chan bool, 3)
	for i := 0; i < requestCount; i++ {
		wg.Add(1)
		ch <- true
		go Read(ch, i)
	}
	wg.Wait()
}

func Read(ch chan bool, i int) {
	fmt.Printf("goroutine_num: %d, go func: %d\n", runtime.NumGoroutine(), i)
	<-ch
	wg.Done()
}

// goroutine_num 1
// goroutine_num 1
// goroutine_num: 4, go func: 0
// goroutine_num: 4, go func: 3
// goroutine_num: 4, go func: 4
// goroutine_num: 4, go func: 5
// goroutine_num: 4, go func: 2
// goroutine_num: 4, go func: 1
// goroutine_num: 4, go func: 8
// goroutine_num: 4, go func: 9
// goroutine_num: 4, go func: 7
// goroutine_num: 4, go func: 6
