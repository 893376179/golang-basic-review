package main

import (
	"fmt"
	"net/http"
	"runtime"
	"sync"
	"time"
)

/*
协程泄漏：goroutine数量一直递增，但不退出不释放资源，导致协程泄漏。
泄漏原因：
	Goroutine 内进行channel/mutex 等读写操作被一直阻塞；
	Goroutine 内的业务逻辑进入死循环，资源一直无法释放；
	Goroutine 内的业务逻辑进入长时间等待，有不断新增的 Goroutine 进入等待。
排查方法：
	调用runtime.NumGoroutine方法打印执行代码前后Goroutine的运行数量，进行前后比较就知道有没有泄漏了。
	使用PProf实时监测Goroutine的数量。
*/

// 泄漏场景一：nil channel(忘记初始化)
func block1() {
	var ch chan int
	for i := 0; i < 10; i++ {
		go func() {
			<-ch
		}()
	}
}

// 泄漏场景二：发送不接收
func block2() {
	ch := make(chan int)
	for i := 0; i < 10; i++ {
		go func() {
			ch <- 3
		}()
	}
}

// 泄漏场景三：接收不发送
func block3() {
	ch := make(chan int)
	for i := 0; i < 10; i++ {
		go func() {
			<-ch
		}()
	}
}

// 泄漏场景四：http request body未关闭（resp.Body.Close() 未被调用时，goroutine不会退出）
var wg sync.WaitGroup

func block4() {
	for i := 0; i < 10; i++ {
		wg.Add(1)
		go func() {
			defer wg.Done()
			// requestWithClose() // 1, 1
			requestWithOutClose()
		}()
	}
}

func requestWithOutClose() {
	_, err := http.Get("https://www.baidu.com")
	if err != nil {
		fmt.Println("err occurred while fatching page, error: ", err.Error())
	}
}

func requestWithClose() {
	resp, err := http.Get("https://www.baidu.com")
	if err != nil {
		fmt.Println("err occurred while fatching page, error: ", err.Error())
		return
	}
	defer resp.Body.Close()
}

// 泄漏场景五：互斥锁忘记解锁
// 第一个协程获取 sync.Mutex 加锁了，但是他可能在处理业务逻辑，又或是忘记 Unlock 了。
// 因此导致后面的协程想加锁，却因锁未释放被阻塞了
func block5() {
	var mutex sync.Mutex
	for i := 0; i < 10; i++ {
		go func() {
			mutex.Lock()
			// fmt.Println("111")
			// mutex.Unlock() // 解决方法
		}()
	}
}

// 泄漏场景六：sync.WaitGroup使用不当
// 由于wg.Add()的数量与wg.Done()数量不匹配，因此在调用wg.Wait()方法后一直阻塞等待。
func block6() {
	for i := 0; i < 10; i++ {
		go func() {
			wg.Add(2) // 创建要等待的goroutine
			wg.Done() // 用来告诉主协程这个go程执行完毕
			wg.Wait()
		}()
	}
}

func main() {
	fmt.Println("before goroutines: ", runtime.NumGoroutine())
	// block1()
	// before goroutines:  1
	// after goroutines:  11

	// block2()
	// before goroutines:  1
	// after goroutines:  11

	// block3()
	// before goroutines:  1
	// after goroutines:  11

	// block4()
	// wg.Wait()
	// before goroutines:  1
	// after goroutines:  21

	// block5()
	// before goroutines:  1
	// after goroutines:  10

	block6()
	// before goroutines:  1
	// after goroutines:  11

	time.Sleep(1 * time.Second)
	fmt.Println("after goroutines: ", runtime.NumGoroutine())

}
