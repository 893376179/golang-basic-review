package main

import "fmt"

func main() {
	// defer延迟函数执行，先进后出
	defer fmt.Println("defer1")
	defer fmt.Println("defer2")
	defer fmt.Println("defer3")
	defer fmt.Println("defer4")
	fmt.Println("begin")
}

// begin
// defer4
// defer3
// defer2
// defer1
