package main

import "fmt"

func main() {
	defer fmt.Println("panic before")
	panic("发生panic")
	defer func() {
		fmt.Println("panic after")
	}() // panic后的defer函数不会被执行（遇到panic，如果没有捕获错误，函数会立刻终止）
}
