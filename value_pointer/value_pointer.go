package main

import "fmt"

// 如果方法的接收者是指针类型，无论调用者是对象还是对象指针，修改都是对象本身，会影响调用者；
// 如果方法的接收者是值类型，无论调用者是对象还是对象指针，修改都是对象的副本，不影响调用者；

/*
	通常我们使用指针类型作为方法的接收者，指针类型能修改调用者的值；
	使用指针类型避免复制原值，在值的类型为大型结构体时，这样做会更加高效。
*/

type Person struct {
	age int
}

// 会修改age的值
func (p *Person) IncrAge1() {
	p.age += 1
}

// 不会修改age的值
func (p Person) IncrAge2() {
	p.age += 1
}

// 如果实现了接收者是值类型的方法，会隐含地也实现了接收者是指针类型的GetAge方法
func (p Person) GetAge() int {
	return p.age
}

func main() {
	// p1是值类型
	p1 := Person{age: 10}
	p1.IncrAge1()
	fmt.Println(p1.GetAge()) // 11

	p1.IncrAge2()
	fmt.Println(p1.GetAge()) // 11

	// p2是指针类型
	p2 := Person{age: 20}
	p2.IncrAge1()
	fmt.Println(p2.GetAge()) // 21
	p2.IncrAge2()
	fmt.Println(p2.GetAge()) // 21
}
