package main

import (
	"bytes"
	"fmt"
	"runtime"
	"strconv"
	"sync"
	"sync/atomic"
)

/*
	可重入锁（递归锁），指同一个线程在外层方法获取锁的时候，
	在进入该线程的内层方法时会自动获取锁，不会因为之前已经获取过还没释放再次加锁导致死锁。
	在Go中由于Mutex重复Lock会导致死锁，所以Go中没有可重入锁。

	思路：记住持有锁的线程；统计重入次数
*/

type ReentrantLock struct {
	sync.Mutex
	recursion int32 // 记录goroutine重入的次数
	owner     int64 //记录当前goroutine的id
}

func GetGoroutineId() int64 {
	var buf [64]byte
	var s = buf[:runtime.Stack(buf[:], false)]
	s = s[len("goroutine "):]
	s = s[:bytes.IndexByte(s, ' ')]
	gid, _ := strconv.ParseInt(string(s), 10, 64)
	return gid
}

func NewReentrantLock() sync.Locker {
	res := &ReentrantLock{
		Mutex:     sync.Mutex{},
		recursion: 0,
		owner:     0,
	}
	return res
}

// 包装一个mutex，实现可重入
type ReentrantMutex struct {
	sync.Mutex
	recursion int32
	owner     int64
}

func (m *ReentrantMutex) Lock() {
	gid := GetGoroutineId()
	// 如果当前持有锁的goroutine就是这次调用的goroutine,说明是重入
	if atomic.LoadInt64(&m.owner) == gid {
		m.recursion++
		return
	}
	m.Mutex.Lock()
	// 获得锁的goroutine第一次调用，记录下它的goroutine id,调用次数加1
	atomic.StoreInt64(&m.owner, gid)
	m.recursion = 1
}

func (m *ReentrantMutex) Unlock() {
	gid := GetGoroutineId()
	if atomic.LoadInt64(&m.owner) != gid {
		panic(fmt.Sprintf("wrong the owner(%d): %d!", m.owner, gid))
	}
	m.recursion--
	if m.recursion != 0 {
		return
	}
	atomic.StoreInt64(&m.owner, -1)
	m.Mutex.Unlock()

}

func main() {
	var mutex = &ReentrantMutex{}
	mutex.Lock()
	mutex.Lock()
	fmt.Println("111")
	mutex.Unlock()
	mutex.Unlock()
}
