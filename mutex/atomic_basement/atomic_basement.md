原子操作：由一个独立的CPU指令代表和完成。原子操作是无锁的，常常直接通过CPU指令直接实现。 事实上，其它同步技术的实现常常依赖于原子操作。

Go atomic包是最轻量级的锁（也称无锁结构），可以在不形成临界区和创建互斥量的情况下完成并发安全的值替换操作，不过这个包只支持int32/int64/uint32/uint64/uintptr这几种数据类型的一些基础操作（增减、交换、载入、存储等）


使用场景：对某个变量并发安全的修改，除了使用官方提供的 `mutex`，还可以使用 sync/atomic 包的原子操作，它能够保证对变量的读取或修改期间不被其他的协程所影响。

atomic 包提供的原子操作能够确保任一时刻只有一个goroutine对变量进行操作，善用 atomic 能够避免程序中出现大量的锁操作。


常见操作：（atomic 操作的对象是一个地址，你需要把可寻址的变量的地址作为参数传递给方法，而不是把变量的值传递给方法）

*     增减操作：Add

    func add(addr *int64, delta int64) {

    atomic.AddInt64(addr, delta)  // 加操作

    fmt.Println("add opts: ", *addr)

}


* 载入操作：Load

    func load(addr *int64) {

    fmt.Println("load opts: ",  atomic.LoadInt64(&addr))

}


* 比较并交换：CompareAndSwap(简称为CAS，可以用来实现乐观锁)

需要注意的是，当有大量的goroutine 对变量进行读写操作时，可能导致CAS操作无法成功，这时可以利用for循环多次尝试。
func compareAndSwap(addr *int64, oldValue int64, newValue int64) {
	if atomic.CompareAndSwapInt64(addr, oldValue, newValue) {
		fmt.Println("cas opts: ", *addr)
		return
	}
}

* 交换Swap

并不管变量的旧值是否被改变，直接赋予新值然后返回背替换的值。

func swap(addr *int64, newValue int64) {
    atomic.SwapInt64(addr, newValue)
    fmt.Println("swap opts: ", *addr)
}


* 存储Store

确保写变量的原子性，避免其他操作读到了修改变量过程中的脏数据。

func store(addr *int64, newValue int64) {
    atomic.StoreInt64(addr, newValue)
    fmt.Println("store opts: ", *addr)
}
