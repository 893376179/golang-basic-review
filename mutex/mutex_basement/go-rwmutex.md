读写互斥锁RWMutex，是对Mutex的扩展。
当一个goroutine获得了读锁后，其他goroutine可以获得读锁，但不能获得写锁；
当一个goroutine获得了写锁后，其他goroutine既不能获取读锁也不能获取写锁。
（只能有一个写或者多个读）

适用场景：读多写少的情况

底层实现结构：
type RWMutex struct {
    w           Mutex  // 复用互斥锁
    writerSem   uint32 // 信号量，用于写等待读
    readerSem   uint32 // 信号量，用于读等待写
    readerCount int32  // 当前执行读的 goroutine 数量
    readerWait  int32  // 被阻塞的准备读的 goroutine 的数量
}

读锁：
    加锁：func (rw *RWMutex) RLock() // 加读锁
    释放锁：func (rw *RWMutex) RUnlock() // 释放读锁

写锁：
    加锁：func (rw *RWMutex) Lock() // 加写锁
    释放锁：func (rw *RWMutex) Unlock() // 释放写锁

互斥锁和读写锁的区别：
    1.读写锁区分读者和写者，而互斥锁不区分。
    2.互斥锁同一时间只能允许一个线程访问该对象，无论读写；读写锁同一时间只允许一个写者，但允许多个读者同时读对象。

