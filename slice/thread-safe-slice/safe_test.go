package threadsafeslice

import (
	"sync"
	"testing"
)

/*
	slice底层并没有使用加锁等方式，不支持并发读写，所以并不是线程安全的。
	使用多个goroutine对类型为slice的变量进行操作，会与预期值不一致；slice在并发执行中不会报错，但数据会丢失。
*/
func TestSliceConcurrencySafe(t *testing.T) {
	a := make([]int, 0)
	var wg sync.WaitGroup
	for i := 0; i < 100; i++ {
		wg.Add(1)
		go func(i int) {
			a = append(a, i)
			wg.Done()
		}(i)
	}
	wg.Wait()
	t.Log(len(a))
}
