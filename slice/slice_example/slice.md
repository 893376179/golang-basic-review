切片是基于数组实现的，它的底层是数组，可以理解为对 底层数组的抽象。
slice数据结构：
type slice struct {
    array unsafe.Pointer
    len int  // 切片长度
    cap int  // 切片容量
}

slice初始化方式：
// 1.直接声明
var slice1 []int
// 2.使用字面量
slice2 := []int{1, 2, 3, 4}
// 3.使用make创建slice
slice3 := make([]int, 3, 5)
// 4.从切片或数组中截取
slice4 := arr[1:3]

初始化slice调用的是runtime.makeslice，makeslice函数的工作主要就是计算slice所需内存大小，然后调用mallocgc进行内存的分配

所需内存大小 = 切片中元素大小 * 切片的容量