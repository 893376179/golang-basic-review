package main

import "fmt"

func main() {
	slice1 := []int{1, 2, 3}
	for i := 0; i < 16; i++ {
		slice1 = append(slice1, i)
		fmt.Printf("addr: %p, len: %v, cap: %v\n", slice1, len(slice1), cap(slice1))
	}
}

// addr: 0xc0000141e0, len: 4, cap: 6
// addr: 0xc0000141e0, len: 5, cap: 6
// addr: 0xc0000141e0, len: 6, cap: 6
// addr: 0xc0000200c0, len: 7, cap: 12
// addr: 0xc0000200c0, len: 8, cap: 12
// addr: 0xc0000200c0, len: 9, cap: 12
// addr: 0xc0000200c0, len: 10, cap: 12
// addr: 0xc0000200c0, len: 11, cap: 12
// addr: 0xc0000200c0, len: 12, cap: 12
// addr: 0xc00007e000, len: 13, cap: 24
// addr: 0xc00007e000, len: 14, cap: 24
// addr: 0xc00007e000, len: 15, cap: 24
// addr: 0xc00007e000, len: 16, cap: 24
// addr: 0xc00007e000, len: 17, cap: 24
// addr: 0xc00007e000, len: 18, cap: 24
// addr: 0xc00007e000, len: 19, cap: 24
